package vip.justlive.frost.core.notify;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import vip.justlive.frost.core.config.Container;
import vip.justlive.frost.core.job.AbstractWrapper;

/**
 * 事件执行包装
 *
 * @author wubo
 */
@NoArgsConstructor
@AllArgsConstructor
public class EventExecuteWrapper extends AbstractWrapper implements Serializable {

  private static final long serialVersionUID = 1L;

  private Event event;

  @Override
  public void doRun() {
    EventListener listener = Container.get().getListener();
    if (listener != null) {
      listener.onEvent(event);
    }
  }
}
