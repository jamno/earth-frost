package vip.justlive.frost.api.model;

import java.io.Serializable;
import lombok.Data;

/**
 * 任务分组
 *
 * @author wubo
 */
@Data
public class JobGroup implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 编号
   */
  private String id;

  /**
   * 分组key
   */
  private String groupKey;

  /**
   * jobKey
   */
  private String jobKey;

  /**
   * job描述
   */
  private String jobDesc;

}
